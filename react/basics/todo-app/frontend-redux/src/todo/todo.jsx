import React from 'react'

import PageHeader from '../template/pageHeader'
import TodoFrom from './todoForm'
import TodoList from './todoList'

export default prop => (
	<div>
		<PageHeader name='Tarefas' small='a fazer' />
		<TodoFrom />
		<TodoList />
	</div>
)