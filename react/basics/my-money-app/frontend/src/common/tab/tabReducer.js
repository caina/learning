const INITIAL_STATE = { selected: '', visible: {} }


export default (state = INITIAL_STATE, action) => {

	const actions = {
		'TAB_SELECTED': { ...state, selected: action.payload },
		'TAB_SHOWED': { ...state, visible: action.payload }
	}

	if (actions[action.type]) {
		return actions[action.type]
	}
	return state
}