import React from 'react'

export default props => (
	<footer className='main-footer'>
		<strong>
			Copyright &copy; 2017
			<a href='http://wwww.douglascaina.com.br' target='_blank'> Caina</a>
		</strong>
	</footer>
)