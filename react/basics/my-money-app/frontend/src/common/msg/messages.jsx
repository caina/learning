import React from 'react'
import ReduxToaster from 'react-redux-toastr'
import 'modules/react-redux-toastr/lib/css/react-redux-toastr.css'

export default props => (
	<ReduxToaster
		timoeOut={4000}
		newestOnTop={false}
		position='top-right'
		preventDuplicates={true}
		transitionIn='fadeIn'
		transitionOut='fadeOut'
		progressBar />
)