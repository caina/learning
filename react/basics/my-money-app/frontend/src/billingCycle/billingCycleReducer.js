const INITIAL_STATE = { list: [] }

export default (state = INITIAL_STATE, action) => {

	const actions = {
		'BILLING_CYCLES_FETCHED': { ...state, list: action.payload ? action.payload.data : undefined }
	}

	if (actions[action.type]) {
		return actions[action.type]
	}
	return state
}