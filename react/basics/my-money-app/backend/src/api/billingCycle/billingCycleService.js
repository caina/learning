const BilliCycle = require('./billingCycle')
const errorHandle = require('../common/errorHandler')

BilliCycle.methods(['get', 'post', 'put', 'delete'])
BilliCycle.updateOptions({ new: true, runValidators: true })
BilliCycle.after('post', errorHandle).after('put', errorHandle)


BilliCycle.route('count', (req, res, next) => {
	BilliCycle.count((error, value) => {
		if (error) {
			res.status(500).json({ errors: [error] })
			return
		}
		res.json({ value })
	})
})

BilliCycle.route('summary', (req, res, next) => {
	BilliCycle.aggregate({
		$project: { credit: { $sum: "$credits.value" }, debt: { $sum: "$debts.value" } }
	}, {
		$group: { _id: null, credit: { $sum: "$credit" }, debt: { $sum: "$debt" } }
	}, {
		$project: { _id: 0, credit: 1, debt: 1 }
	}, (error, result) => {
		if (error) {
			res.status(500).json({errors: [error]})
		} else {
			res.json(result[0] || {credit: 0, debt: 0})
		}
	})
})

module.exports = BilliCycle