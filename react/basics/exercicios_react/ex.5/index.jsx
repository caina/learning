import React from 'react'
import ReactDOM from 'react-dom'
import ClassComponent from './classComponent'

ReactDOM.render(
    <ClassComponent label='Contador das galaxias' initialValue={10} />
, document.getElementById('app'))