export default class Pessoa {

    constructor(nome) {
        this.nome = nome
    }

    toString() {
        return `Pessoa linda: ${this.nome}`
    }
}