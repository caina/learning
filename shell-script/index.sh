#!/bin/bash

SCRIPTS=$(ls ./excercises)

for SCRIPT in $SCRIPTS
do
    QUALIFIED_PATH="./excercises/$SCRIPT"
    chmod +x $QUALIFIED_PATH

    printf "\n\n Executing: ${SCRIPT} "
    $QUALIFIED_PATH $1
done