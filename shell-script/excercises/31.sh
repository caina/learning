#!/bin/bash -v

LINE_NUMBER=0
while read LINE
do
    echo "${LINE_NUMBER}: $LINE"
    ((LINE_NUMBER++))
done <~/passwords