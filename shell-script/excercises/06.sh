#!/bin/bash
# read -p "enter a file or directory" PATHTO
PATHTO=~/Workspace

if [ -f $PATHTO ]
then
    printf "$PATHTO is a regular file"
elif [ -d $PATHTO ]
then
    ELEMENTS=$(ls $PATHTO)
    for ELEMENT in $ELEMENTS
    do
        printf "\n $ELEMENT"
    done
fi

