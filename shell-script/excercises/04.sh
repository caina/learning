#!/bin/bash
if [ -e /etc/shadow ]
then
    printf "\n Shadow passwords are enabled"
    if [ -w /etc/shadow ]
    then
        printf "\n You have permission to edit /etc/shadow"
    else
        printf "\n You dont have permission to edit"
    fi
fi