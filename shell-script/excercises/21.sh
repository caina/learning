#!/bin/bash
function COUNT_FILES() {
    local COUNT=$(ls $1 | wc -l)
    printf "\n Total of: $COUNT files"   
}
COUNT_FILES ~/Workspace