#!/bin/bash

cat /etc/shadow
EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]
then
    printf "Command succeeded"
    exit 0
elif [ "$EXIT_STATUS" -gt "0" ]
then
    printf "Command failed"
    exit 1
fi