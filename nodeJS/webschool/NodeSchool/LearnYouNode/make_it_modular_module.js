function filter_list(folder_path, filter, callback){
    var fs = require('fs');
    var path = require('path');
    
    var files_founded = [];
    
    fs.readdir(folder_path, (err, list)=>{
        if(err){
            return callback(err, null);
        }
        
        list.forEach((list_file)=>{
           if(path.extname(list_file).indexOf(filter) >= 1){
               files_founded.push(list_file);
           }
        }); 
        return callback(null,files_founded);
    });
}

module.exports = filter_list;