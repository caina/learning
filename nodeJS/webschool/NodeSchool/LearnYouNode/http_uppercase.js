/*
Write an HTTP server that receives only POST requests and converts  
  incoming POST body characters to upper-case and returns it to the client.  
   
  Your server should listen on the port provided by the first argument to  
  your program.
  
 through2-map Consegue mudar os streams, ai eu mandei ele modificar os chuncks do request que é a entrada
 e mandar estes dados pro request usando um pipe.

  
  Esses pipes são muito loucos :D
*/

const http = require('http');
const map = require('through2-map');

http.createServer((req,res)=>{
    if(req.method == 'POST'){
        
        req.pipe(map(function (chunk) {  
            return chunk.toString().toUpperCase();
        })).pipe(res)  
        
        req.on('end',()=>{
            res.end();
        })
    }
}).listen(process.argv[2]);
