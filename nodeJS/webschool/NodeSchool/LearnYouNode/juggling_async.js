/*
This problem is the same as the previous problem (HTTP COLLECT) in that  
  you need to use http.get(). However, this time you will be provided with  
  three URLs as the first three command-line arguments.  
   
  You must collect the complete content provided to you by each of the URLs  
  and print it to the console (stdout). You don't need to print out the  
  length, just the data as a String; one line per URL. The catch is that you  
  must print them out in the same order as the URLs are provided to you as  
  command-line arguments.  

*/

var http = require('http');
var concatStream = require('concat-stream');
var request_data = [];

function get_data(index){
    http.get(process.argv[2+index], (request)=>{
        request.setEncoding('utf8');
        request.pipe(concatStream((chunk)=>{
            
            request_data[index] = chunk.toString();
            
            if(index ==2){
                print_result();
            }
        }));
    });
}

function print_result(){
    for(var t=0; t<request_data.length;t++){
        console.log(request_data[t]);
    }
}

for(var i=0;i<3;i++){
    get_data(i);
}