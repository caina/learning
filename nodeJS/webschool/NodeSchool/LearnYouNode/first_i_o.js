/*
      
  Write a program that uses a single synchronous filesystem operation to  
  read a file and print the number of newlines (\n) it contains to the  
  console (stdout), similar to running cat file | wc -l.  
   
  The full path to the file to read will be provided as the first  
  command-line argument (i.e., process.argv[2]). You do not need to make  
  your own test file. 
  
  the minus 1 is because length do not count the 0 as first
*/
const fs = require('fs');
var document_content = fs.readFileSync(process.argv[2]);
console.log(document_content.toString().split("\n").length-1);