/*
LEARNYOUNODE HTTP JSON API SERVER 

Write an HTTP server that serves JSON data when it receives a GET request  
  to the path '/api/parsetime'. Expect the request to contain a query string  
  with a key 'iso' and an ISO-format time as the value.  
   
  For example:  
   
  /api/parsetime?iso=2013-08-10T12:10:15.474Z  
   
  The JSON response should contain only 'hour', 'minute' and 'second'  
  properties. For example:  
   
     {  
       "hour": 14,  
       "minute": 23,  
       "second": 15  
     }  
   
  Add second endpoint for the path '/api/unixtime' which accepts the same  
  query string but returns UNIX epoch time in milliseconds (the number of  
  milliseconds since 1 Jan 1970 00:00:00 UTC) under the property 'unixtime'.  
  For example:  
   
     { "unixtime": 1376136615474 }  
   
  Your server should listen on the port provided by the first argument to  
  your program.  
   

*/

var http = require('http');
var url = require('url');


http.createServer((req,res)=>{
    var query_params = url.parse(req.url,true);
    var date_income = new Date(query_params.query['iso']);
    var json_output;
    
    res.writeHead(200, { 'Content-Type': 'application/json' });
    switch(url.parse(req.url).pathname){
        case '/api/parsetime':
            
            json_output = {hour:date_income.getHours(),minute:date_income.getMinutes(), second:date_income.getSeconds()};
            break;
        case '/api/unixtime':
            json_output = {unixtime : date_income.getTime()};
            break;
    }
    res.end(JSON.stringify(json_output));
    
}).listen(process.argv[2]);