/*
TOWER-OF-BABEL
────────────────
 CLASS EXTEND
 Exercise 3 of 12
 
 Rewrite the classes that are written below in the prototype and util.inherit fashion with the new ES6 syntax.
 
 Nota:
 O construtor para ser chamado no php, você deveria usar um parent::construct();
especificando que era o construtor do pai, no ES6, é só declarar o super(), achei meio confuso
mas muito melhor do que usar prototype, outra coisa que me chamou a atenção, é a possibilidade de concatenar
valores numa string sem quebrar ela, mesmo estes valores sejam originários de uma função

em PHP podiamos fazer isso tbm, dês de que não sejam valores de uma classe, nem que seja valores de uma função.

*/

class Character{
    
    constructor(x,y){
        this.x = x;
        this.y = y;
        this.health_ = 100;
    }
    
    damage(){
        this.health_ = this.health_ - 10;
    }
    
    getHealth(){
        return this.health_;
    }
    
    toString(){
        return `x: ${this.x} y: ${this.y} health: ${this.getHealth()}`;
    }
}

class Player extends Character{
    
    constructor(x,y,name){
        super(x,y);
        this.name = name;
    }
    
    move(dx,dy){
        this.x += dx;
        this.y += dy;
    }
    
    toString(){
        return `name: ${this.name} ${super.toString()}`;
    }
}

var x = process.argv[2];
var y = process.argv[3];
var name = process.argv[4];
var character = new Character(+x, +y);
character.damage();
console.log(character.toString());
var player = new Player(+x, +y, name);
player.damage();
player.move(7, 8);
console.log(player.toString());