/*
Create a javascript program that takes the the first command-line argument and
outputs it right after a "Hello " String using ES6 template strings.

ES6 está usando o caracter ` como aspas
*/

var received_argument = process.argv[2];
console.log(`Hello ${received_argument}`);