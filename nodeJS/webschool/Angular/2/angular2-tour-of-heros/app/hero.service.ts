import {Injectable} from 'angular2/core'
import {Hero} from './model/hero'
import {HEROES} from "./data/mock-heroes"

@Injectable ()

export class HeroService{
  getHeroes(){
    return Promise.resolve(HEROES);
  }
}
