var scotchApp = angular.module('scotchApp',['ngRoute']);

scotchApp.config(function($routeProvider){
	$routeProvider
		.when("/",{
			templateUrl: 'pages/home.html',
			controller: 'mainController'
		})
		.when("/about",{
			templateUrl: 'pages/about.html',
			controller: 'aboutController as about'	
		})
		.when("/contact",{
			templateUrl: 'pages/contact.html',
			controller: 'contactController as contact'			
		});
});

scotchApp.controller('mainController',function(){
	this.message = "Everyone come and see how good i look";
});

scotchApp.controller('aboutController',function(){
	this.message = "About page!";
});

scotchApp.controller('contactController',function(){
	this.message = "Contact page lalalala!";
});