use projeto-final;

function random_name(){
	names = ["Myong Schiller", "Ashleigh Turpen", "Domitila Goyette", "Angla Gott", "Zofia Moronta", "Talisha Marengo", "Horacio Caves", "Erminia Parish", "Lashawna Berggren", "Jewell Santos", "Terrence Allan", "Maribel Linsey", "Trisha Westbrook", "Shelly Platter", "Liz Shelman", "Melodie Albaugh", "Annamarie Sarcone", "Judith Boggan", "Janee Greenhalgh", "Christiane Goodrow", "Minnie Herman", "Edythe Toy", "Glendora Mellen", "Marta Pehrson", "Celestine Engelmann", "Natosha Parm", "Jalisa Alvarenga", "Marty Sheaffer", "Clarice Bibby", "Barbara Lanser", "Zane Nadal", "Rosamaria Waldrip", "Ginger Redick", "Meredith Hulin", "Hermina Gulino", "Selene Sheehan", "Linn Blakeney", "Rosalba Venegas", "Colby Pedigo", "Jame Pratico", "Madalyn Gaskins", "Roma Duwe", "Reggie Catanzaro", "Sabina Sifford", "Jettie Vanhoy", "Alfonso Cardon", "Deirdre Rader", "Keila Mccaffery", "Mackenzie Mabery", "Kathrine Cadena"];
	return names[Math.floor(Math.random()*names.length)];
}

function random_word(){
	words = ["vansittart", "decelerator", "gerbil", "khalkha", "calibered", "kolbe", "fibiger", "homoiothermism", "somatist", "koestler", "loblolly", "pawl", "amidship", "reedling", "reinvolve", "nightclubclubbed", "cautionary", "untraded", "nacelle", "misemphasizing", "ishtar", "precorrupt", "hebron", "unmuffle", "bimotored", "myalgic", "muffin", "fence", "croupily", "isoperimetric", "satellite", "renegade", "saturnine", "lowness", "precordial", "supersalesman", "aboriginality", "ouachita", "nonreparation", "unreposeful", "husklike", "paronomastically", "ungestural", "gasometer", "nonadjudication", "mull", "accurateness", "jaggiest", "nannoplankton", "pich", "deephaven", "lacily", "foetuses", "softwood", "anhwei", "groovy", "outcome", "subcurate", "reinterview", "caspian", "zoar", "katakana", "histie", "nondiffractive", "hairpiece", "lecheates", "unresistant", "hateful", "boy", "unsurliness", "alchemy", "rusticator", "frs", "interjectionally", "nonmetaphoric", "hughes", "windily", "maturated", "aethra", "brachycephaly", "inphase", "unportentous", "dribblet", "megalecithal", "idiocrasy", "ballance", "misassign", "abound", "hightstown", "heterodoxies", "assentiveness", "noncombustible", "walies", "pontianak", "sandy", "rowdyism", "rutherford", "unrigging", "hylomorphism", "unsystematizing"];
	return words[Math.floor(Math.random()*words.length)];
}

function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function createUser(){
	var User = {
		name: random_name(),
		bio: "aleatorio de "+random_name(),
		date_register: Date.now(),
		avatar_path: "",
		Auth:{
			username: "user_"+random_name().replace(" ","_"),
			email: random_name().replace(" ","_")+"@gmail.com",
			password: makeid(),
			last_acess: Date.now(),
			online: true,
			disabled: false,
			hash_token: makeid(),
		},
		Settings:{
			background_path: "",
		}
	};
	return User;
}


function createProject(){
	var Project = {
		name: random_word(),
		description: "descricao do "+random_word(),
		date_begin:Date.now(),
		date_dream:Date.now(),
		date_end:Date.now(),
		visible: true,
		relocated: false,
		expired: false,
		visualizable_mod:1,
		Members:[],
		Goals:[{
			name:random_word(),
			description:"descricao do "+random_word(),
			date_begin:Date.now(),
			date_dream:Date.now(),
			date_end:Date.now(),
			visible: true,
			relocated: false,
			expired: false,
			Tags:[{tag_name:random_word()},{tag_name:random_word()},{tag_name:random_word()}],
			Goal_historic:[],
			Activities:[{
				name: random_word(),
				description: "descricao do "+random_word(),
				date_begin:Date.now(),
				date_dream:Date.now(),
				date_end:Date.now(),
				relocated: false,
				expired: false,
			}]
		}],
		Tags:[{tag_name:random_word()},{tag_name:random_word()},{tag_name:random_word()}]
	}
	return Project;
}

// // CREATE
// 1. Cadastre 10 usuários
// for (var i = 0; i < 10; i++) {
// 	db.users.insert(createUser());
// };
// 2. Cadastre 5 empresas
// for (var i = 0; i < 5; i++) {
// 	db.projects.insert(createProject());
// };

// BUSCAR

// // 1. Buscar informações dos membros de um projeto buscando pelo seu nome sem case sensitive
// db.users.find(
// {
// 	_id:{
// 		$in:db.projects.findOne({name:/accurateness/i}).Members.map(function(elem){
// 			return elem.user_id
// 		})
// 	}
// })


// //  2.Buscar projetos com a tag kolbe
// db.projects.find({Tags:{$exists:'kolbe'}})

// // 3. Liste apenas os nomes de todas as atividades para todos os projetos.

// // 4. Liste todos os projetos que não possuam uma tag.
// db.projects.find({Tags:{$exists:false}})

// //5. Liste todos os usuários que não fazem parte do primeiro projeto cadastrado.
// db.users.find({
// 	_id:{
// 		$nin:db.projects.findOne().Members.map(function(element){return element.user_id})
// 	}
// })


// UPDATE
// // 1. Adicione para todos os projetos o campo views: 0.

// // 2. Adicione 1 tag diferente para cada projeto.

// var projects = db.projects.find();
// for (var i = 0; i < projects.length(); i++) {
// 	db.projects.update({_id:projects[i]._id},{$pushAll:{Tags:[{tag_name:"PENTAKILL"}]}});
// };

// // 3. Adicione 2 membros diferentes para cada projeto.
// function get_random_user(){
// 	return db.users.find().limit(-1).skip(Math.random()*db.users.count()).next();
// }
// var projects = db.projects.find();
// for (var i = 0; i < projects.length(); i++) {
// 	var current_project = projects[i];
// 	var users = [];
// 	for (var t = 0; t < 2; t++) {
// 		users[t] = {user_id:get_random_user()._id};
// 	};
// 	db.projects.update({_id:current_project._id},{$pushAll:{Members:users}});
// };

// //4. Adicione 1 comentário em cada atividade, deixe apenas 1 projeto sem
var projects = db.projects.find();
for (var i = 0; i < projects.length(); i++) {
	var project = projects[i];
	for (var i = 0; i < project.Ativities.length; i++) {
		project.Ativities[i]
	};
};

// //5.Adicione 1 projeto inteiro com UPSERT.