# MongoDB - Aula 06 - Exercício
autor: Douglas Caina

## 1 Adicionar 2 indexes na tabela pokemon, no name e em dois outros campos juntos, fazer 2 querys, uma com indice e outra sem, colocar o explain para mostrar a melhora


```
db.pokemons.find({name:'Pikachu'})
tempo: 0.002

db.pokemons.find({$and:[{attack:{$gt:50}},{defense:{$gt:40}}]})
tempo: 0.001
```

Com Index


```
db.pokemons.find({name:'Pikachu'})
tempo: 0

db.pokemons.find({$and:[{attack:{$gt:50}},{defense:{$gt:40}}]})
tempo: 0.001
```
