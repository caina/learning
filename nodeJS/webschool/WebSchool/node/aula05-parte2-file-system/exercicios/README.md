#Exercícios
Aula que continua o FileSystem da 04, iremos fazer:

- Criar um Arquivo
- Ler um arquivo
- Editar o conteúdo desse arquivo
- Deletar
- Renomear

## Desafio
Criar um servidor web de arquivos estáticos:

- CSS
- HTML
- JS
