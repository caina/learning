/*
*
* Arquivo que vai rodar o desafio do curso Webschool e ainda fazer todos os outros exercicios
* Como vai funcionar:
*
* Vamos criar um serviço que vai rodar esperando conecxão
* Quando ele receber, vai criar uma pasta com a data, cada dia uma nova
* Nesta pasta vamos colocar um arquivo cssou js, ler e colocar o conteudo no browser, quando a aplicação iniciar,
* vamos deletar todas as pastas e arquivos
* quando trocar a data, renomearemos a pasta
*
*index.css
index.html
*
*/
'use-strict'

const fs = require('fs');
const http = require('http');

http.createServer((req,res)=>{
  switch (req.url) {
    case '/js':
      create_folder("js",(err)=>{
        if(err) console.log(err);
        create_file("js",".js","console.log('arquivo criado dinamicamente')",(err,data)=>{
          if(err) console.log(err);
          res.writeHead(200,{'Content-type':'text/javascript'});
          res.end(data);
        });
      });
      break;
    case '/css':
      //Cria estrutura de pastas primeiro
      create_folder('css',function(err){
        if(err) console.log(err);

        create_file("css",".css","body{background-color:red};h1{color:blue};",function(err,data){
          if(err) console.log(err);
          res.writeHead(200,{'Content-type':'text/css'});
          res.end(data);
        });
      });
      // escrever arquivo
      break;
      case '/html':
        fs.readFile('./index.html',{encodign:'utf8'},(err, data)=>{
          res.writeHead('200',{'Content-type':'text/html'});
          res.end(data);
        });
        break;
  }
}).listen(3000,()=>{
  // arquivos velhos
  remove_folder("./assets");
});

function remove_folder(path){
  fs.readdirSync(path).forEach(function(file, index){
    var current_path = `${path}/${file}`;
    if(fs.lstatSync(current_path).isDirectory()){
      remove_folder(current_path);
    }else{
      fs.unlinkSync(current_path);
    }
  });
}
// funcoes

/*
Cria as pastas que o usuario pede, caso nao exista
e chama o callback
*/
function create_folder(path,callback){
  try{
    fs.mkdirSync(`./assets/${path}`);
  }catch(e){
    if ( e.code != 'EEXIST' ) throw e;
  }
  callback(null);
}

function create_file(path, extention, file_content, callback){
  var tmp_file_name = new Date().getTime();
  tmp_file_name = `./assets/${path}/${tmp_file_name}${extention}`;
  fs.writeFile(tmp_file_name, file_content,(err,result)=>{
    if(err){
      callback(err,null);
    }else{
      fs.readFile(tmp_file_name,{encodign:'utf8'},(err,data)=>{
        if(err){
          callback(err,null);
        }else{
          callback(null, data);
        }
      })
    }
  });

}
