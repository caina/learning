## Globals
Globals são como as magic words do php, tem acesso á variáveis sem precisar usar o require
ex:
```
'use-strict';

console.log(__dirname);
console.log(__filename);
```

## Process
São objetos globais e é uma instancia de event emiter, ou seja o que emitem eventos
```
console.log(process.execPath);
console.log(process.cwd());
```

Processo para saber quando a aplicação é morta
```
process.on("SIGINT",()=>{
  console.log("sakiiii!");
  process.exit(0);
})
```
