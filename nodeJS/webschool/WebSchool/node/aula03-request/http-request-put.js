'use strict'

const http = require('http');
const querystring = require('querystring');
const postData = querystring.stringify({
	name: 'Editado Douglas Caina',
	type: 'Web Developer'
});
const options = {
	host:'webschool-io.herokuapp.com',
	method: 'PUT',
	path: '/api/pokemons/568bc9fe8f704b11001bb4b5',
	headers:{
		'Content-Type':'Application/x-www-form-urlencoded',
		'Content-Lenght':postData.length
	}
};

function callback(res){
	console.log('STATUS: '+res.statusCode);
	console.log('HEADERS: '+JSON.stringify(res.headers));

	let data = '';

	res.setEncoding('utf8');
	res.on('data',(chunk)=>{
		data+=chunk;
	});
	res.on('end',()=>{
		console.log('Dados finalizados ',data);
	});
}

const req = http.request(options, callback);
req.on('error',(e)=>{
	console.warn("OPPSS!: "+e.message);
})
//envia os dados
req.write(postData);
req.end();



