'use strict'

const http = require('http');

http.request(
{
	host:'webschool-io.herokuapp.com',
	method: 'GET',
	path: '/api/pokemons',
},(res)=>{
	let data = '';
	res.setEncoding('utf8');
	res.on('data',(chunk)=>{
		data+=chunk;
	});
	res.on('end',()=>{
		data = JSON.parse(data);
		for (var i = data.length - 1; i >= 0; i--) {
			delete_nodes(data[i]._id);
		};
	});
}).end();

function delete_nodes(node_id){
	http.request({
		host:'webschool-io.herokuapp.com',
		method: 'DELETE',
		path: '/api/pokemons/'+node_id,
		headers:{
			'Content-Type':'Application/x-www-form-urlencoded',
			'Content-Lenght':0
		}
	},(res)=>{
		res.on('end',()=>{
			console.log(node_id+' deletado!');
		});
	}).end();
}