var http = require("http"), url = require('url');

http.createServer(function (req,res) {
	var result = url.parse(req.url, true);
	res.write("<html><body>");
	res.write("<h1>query string</h1>");
	res.write("<ul>");
	for(var key in result.query){
		res.write("<li>"+key+":"+result.query[key]+"</li>");
	}
	res.write("</ul>");
	res.write("</body></html>");
	res.end();
}).listen(3000, function(){
	console.log("aios silver!");
});