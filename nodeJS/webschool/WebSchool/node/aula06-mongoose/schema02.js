'use-strict';
// require('./config');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const _schema = {
  name:String,
  pokemons: [{type: Schema.Types.ObjectId,ref:'pokemons'}]
}

const pokemonsSchema = new Schema(_schema);
const data = {name:"Caina",pokemons:['564b1dad25337263280d047c','564b1dae25337263280d0488']}

const Model = mongoose.model('treinadores',pokemonsSchema);
const poke = new Model(data);
poke.save(function(err,data){
  if(err) return console.log("Erro: ",err);
  console.log("inserted",data);
});
module.exports = pokemonsSchema;
