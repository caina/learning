'use-strict';

const mongoose = require('mongoose');
const dbURI = "mongodb://localhost/be-mean";

mongoose.connect(dbURI,function(err){
  if(err) console.log(err);
});

mongoose.connection.on("connected",function(){
  console.log("connectou em: "+dbURI);
});
mongoose.connection.on("error",function(){
  console.log("erro");
});
mongoose.connection.on("disconnected",function(){
  console.log("desconectou");
});
mongoose.connection.on("open",function(){
  console.log("abriu");
});

process.on("SIGINT",function(){
  mongoose.connection.close(function(){
      process.exit(0);
  });
});
