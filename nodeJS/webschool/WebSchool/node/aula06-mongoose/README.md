## Mongoose
É para trabalhar com o mongo diretamente do node.js, e ainda prove funcionalidades que o mongoDB não possui
Ele possui Schemas! Tudo no Mongoose começa com um Schema, basicamente ele é um ORM para documentos

## Exemplo de Schema

Pra criar um esquema, fica desta maneira

```
const pokemonSchema = new Schema({
  name: String,
  description: String,
  type: String,
  attack: Number,
  defense: Number,
  height: Number
});
```

## Conexção
Para conectar precisa de uma string de conexção:

```
mongoose.connect("mongodb://localhost/be-mean");
```

e podemos gerenciar os erros:

```
mongoose.connection.on("connected",function(){
});
mongoose.connection.on("error",function(){
});
mongoose.connection.on("disconnected",function(){
});
mongoose.connection.on("open",function(){
});
```

E quando você finaliza o processo

```
process.on("SIGINT",function(){
  mongoose.connection.close(function(){
      process.exit(0);
  });
});
```

## Default

Podemos ter campos padrões no Mongoose

```
  created_at:{type:Date, default:Date.now}
```
Não use Date.now() use Date.now, porque se não quando o model for criado, ele vai dar o eval da função e não vai trazer a data correta.

Observamos que ele salva diferentemente dos outros, como ISODATE, o default é um atalho para salvar isto

## Tipos de campos
O mongoose aceita os tipos:

- String: Apenas texto
- Number: Números negativos, positivos, decimais e frações (1/2)
- Date: Aceita datas Date.now
- Buffer: Dados Binários
- Boolean: né?
- Mixed: tudo, aceita tudo
- ObjectId: 1 pra n
- Array: {campo: [_tipo_]}

Ao inserir um item, aparece um atributo ```__v```, que é para versionar internamente, caso tenha alteração concorrente
