'use-strict';
require('./config');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const pokemonSchema = new Schema({
  name: String,
  description: String,
  type: String,
  attack: Number,
  defense: Number,
  height: Number,
  created_at:{type:Date, default:Date.now}
});

// console.log("Schema",pokemonSchema);
const data = {name:"Cainamon"};
var Model = mongoose.model("pokemons",pokemonSchema);
var poke = new Model(data);
poke.save(function(err,data){
  if(err) return console.log("ERRO: ",err);
  console.log("Inserted: ",data);
});

module.exports = pokemonSchema;
