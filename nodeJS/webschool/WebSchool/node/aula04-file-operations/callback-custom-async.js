/*
* aula 2 de callbabks
*
*/

function sum(num1, num2,callback){
	setTimeout(function(){
		if(typeof num1 == "number" && typeof num2 == "number"){
			var result = num1+num2;
			callback(null,result);
		}else{
			var err = new Error("apenas numeros");
			callback(err,null);
		}
	}, 10);
};

function minus(num1, num2,callback){
	setTimeout(function(){
		if(typeof num1 == "number" && typeof num2 == "number"){
			var result = num1-num2;
			callback(null,result);
		}else{
			var err = new Error("apenas numeros");
			callback(err,null);
		}
	}, 20);
};



minus(9,2,function(err, result){
	if(err){
		console.log(err);
	}else{
		console.log(result);
	}
	sum(1,2,function(err,result){
		console.log(result);
	})
});
