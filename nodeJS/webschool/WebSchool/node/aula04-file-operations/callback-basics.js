/***

function err,result)
**/

function sayName(name, callback){
	if(typeof name == 'string'){
		return callback(null, name);
	}else{
		var err = new Error("Ops! o nome tem que ser string");
		return callback(err,null);
	}
}

sayName(5,function(err,name){
	if(err){
		console.log(err);
	}else{
		console.log(name);
	}
});
