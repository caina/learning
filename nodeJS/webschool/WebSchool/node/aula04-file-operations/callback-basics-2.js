/*
* aula 2 de callbabks
*
*/

function sum(num1, num2,callback){
	if(typeof arguments[0] == "number" && typeof arguments[1] == "number"){
		var result = num1+num2;
		callback(null,result);
	}else{
		var err = new Error("apenas numeros");
		callback(err,null);
	}
};

sum('a',2,function(err, result){
	if(err){
		console.log(err);
	}else{
		console.log(result);
	}
});

sum(9,2,function(err, result){
	if(err){
		console.log(err);
	}else{
		console.log(result);
	}
});