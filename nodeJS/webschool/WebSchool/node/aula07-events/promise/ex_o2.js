'use strict';

const promiseAll  = require("./promise-all.js");
const readFile    = require("./fs-promise.js");

promiseAll([
  readFile("./assets/person.json"),
  readFile("./assets/person.json")
])
.then(res=> console.log(res))
.catch(err=> console.log(err));


// Ou pode ser executado desta forma
Promise.all([
  readFile("./assets/person.json"),
  readFile("./assets/person.json")
])
.then(res=> console.log(res))
.catch(err=> console.log(err));
