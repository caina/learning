'use strict';

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/be-mean');
const Pokemon = require('../mongoose-callback-sample/models/pokemons');

let promise = Pokemon.find({name:'caina'}).exec();
promise.then(success, error);

function success(data){
  console.log(data);
}

// Pokemon.create({name:'caina'});

function error(data){
  console.error(data);
}
