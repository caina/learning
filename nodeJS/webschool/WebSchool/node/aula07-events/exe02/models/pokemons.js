'use strict';

const mongoose = require('mongoose');
const util = require('util');

function pokemonHandler(){
  let Schema = mongoose.Schema;
  const ObjectId = Schema.ObjectId;

  const schema = new Schema({
    id          : ObjectId,
    name        : {type: String, trim: true},
    type        : {type: String, trim: true},
    atack       : {type: Number},
    defence     : {type: Number},
    height      : {type: Number},
    description : {type: String, trim: true},
  });

  schema.pre('find', function(next){
    this.start = Date.now(),
    util.log('Finding... ');
    // passa pra proxima funcao do node
    next();
  });

  schema.post('find', function(result){
    setTimeout(function(){
      console.log('finding end');
    },1000);
  });

  return mongoose.model('Pokemon', schema);
}

module.exports = exports = pokemonHandler();
