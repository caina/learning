'use-strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _schema = {
  name: String
}

const pokemonSchema = new Schema(_schema);
const PokemonModel = mongoose.model('Pokemon',pokemonSchema);

const query = { attack:{$gt: 90}};


mongoose.connect("mongodb://localhost/be-mean",function(err){
  if(err) console.log(err);

  PokemonModel.find(query,{name:1,_id:0}, function(err, data){
    if(err) return console.log("Erro: ",err);
    console.log("Retornou: ",data);
  });

});


module.exports = PokemonModel;
