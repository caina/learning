# Aula 5

## NPM
Npm é um gerenciador de pacotes do node

## Dependencias
 Nunca envie o conteúdo da pasta node_modules que será gerada ao instalar um módulo, adicionar sempre no .gitignore

 Fora o NPM podemos usar gerenciadores de versões, como:
 - N
 - NVM

 ## Comandos:

```
npm init
```
Inicia um projeto localmente, ele vai pedir várias perguntas que irão gerar um package.json

## Sobre
O mais importante é o nome, este campo é obrigatório e único, existem regras para criar o nome, delas:

- Precisa ser menos que 214 caracteres
- Não pode começar com . ou _
- Novos packagesnão podem ter letras maiúsculas
- Não pode conter caracteres não url-safe

tente não usar o nome de um mesmo módulo do core nodenão coloque js ou node no nome
não será passado como um argumento para o require, por isso deve ser algo curto
verifique no site do npm para ver se não existem outros iguais

## Instalando um módulo

comando :

```
npm install
```

Serve para instalar algum pacote ou as dependencias listadas no package.json,
quando vai instalar um módulo global

```
npm install -g _nome_do_modulo_
```
Global é para executar em linha de comando, o que o local não libera, tipo o que tu pode fazer com o Grunt

## Módulos que vamos precisar ter instalado

- Gulp
- nodemon
- mocha
- express-generator

## Npm install save

```
npm install --save _nome_pacote_@versao
```
Adiciona o modulo á listagem de dependencias do package.json,


A versão que fica depois do @, pode ser gerenciado com a versão da seguintes maneiras:
- '~' pega a versão equivalente
- '^' Versão compativel
- nenhum caracter é versão exata
- '>' Versão maior
- '>=' Versão maior ou igual
- '<' Versão menor
- '<=' Versão menor ou igual


## Dependencias de produção

Para instalar as dependencias de produção:
```
npm install production
npm install --dev
```

se usar npm install --save-dev você salva dependências apenas para desenvolvimento, pra tirar os módulos de testes

se usar
```
npm install --save-optional
```
fica como dependencia opcional, que não interfere na aplicação

## Rodando
Podemos rodar scripts com o comando
```
npm run
```
para automatizar processos de um projeto.
Criamos uma pasta chamada npm, dentro dela
```
npm init
```

Criamos um arquivo chamado scripts.js com o conteúdo:
```
console.log("rodei");
```
e no nosso package.json, vamos adicionar o objeto:
```
"scripts": {
  "roda":"node scripts.js"
}
```
e para executar isso iremos chamar
```
npm run roda
```

Maior vantagem disso é criar rotinas de deploy, acessar o servidor, fazer um backup do banco etc
