const mongoose     = require('mongoose');
const Schema       = mongoose.Schema;
const postSchema   = require('./user-posts');
const faker        = require('faker');

const validateEmail = function(email){
  const re = /^\w+([\.-]?w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const idadeValidate = function(idade){
  return idade >= 18;
};

function toLower(v){
  return v.toLowerCase();
}
function toUpper(v){
  return v.toUpperCase();
}

const uniqueEmail = function(){
  return faker.internet.email();
}

const _schema = new Schema({
 name:{
   first: {type: String, trim: true},
   last: {type:String, trim: true},
 },
 age:{
   type: Number,
   validate: {
     validator: idadeValidate,
     message: "Idade {VALUE} não permitida"
   }
 },
 email:{
   type: String,
   trim: true,
   set: toLower,
   get:toUpper,
  //  default:uniqueEmail,
  required: true,
  index:true,
   validate: [validateEmail,'Preencha com um email válido: {VALUE-}']
 },
 requisitos:{
   type: String
 },
 posts:[postSchema],
 created_at:{
   type: Date,
   default: Date.now
 }
});

_schema
  .virtual('name.full')
  .get(function() {
    if( this.name.first !== undefined && this.name.last !== undefined){
      return this.name.first+" "+this.name.last
    }
    return "";
  });

_schema
  .virtual('name.initials')
  .get(function() {
    if( this.name.first !== undefined && this.name.last !== undefined){
      return this.name.first[0]+"."+this.name.last[0]+"."
    }
    return "";
  });


_schema.index({name:-1, email: 1});
_schema.methods.findByEmail  = function (cb){
  return this.model('user').find({email:this.email},cb);
};


module.exports = _schema;
