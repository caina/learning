'use strict';

const mongoose = require('mongoose');
const _schema = {
  title: {type:String, required: true},
  body: String,
  date: {type: Date, default: Date.now()}
}

module.exports = new mongoose.Schema(_schema);
