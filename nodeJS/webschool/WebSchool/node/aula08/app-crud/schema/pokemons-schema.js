const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _schema = {
  name        : {type:String},
  description : {type:String},
  type        : {type:String},
  attack      : {type:Number},
  defense     : {type:Number},
  height      : {type:Number},
  created_at  : {type: Date, default: Date.now}
};

module.exports = new Schema(_schema);
