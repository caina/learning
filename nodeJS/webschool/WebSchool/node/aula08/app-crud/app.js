'use strict';

require('./db/config');
const PokemonController = require('./pokemon-controller');

var pokemon_create =  {
    name: 'Tiraromom',
    types: [ 'developer', 'node','javascript' ],
    speed: 190,
    hp: 35,
    height: 4,
    defense: 40,
    attack: 55
  };

var pokemon_get = {name: 'Douglas Caina pokemon'};

PokemonController.retrieve(pokemon_get);
