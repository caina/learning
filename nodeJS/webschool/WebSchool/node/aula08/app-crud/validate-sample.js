'use strict';

// const mongoose = require('mongoose').connect(require('./db/database_options').mongoDbPath);
const http          = require('http');
const fs            = require('fs');
const queryString   = require('querystring');
const url           = require('url');

const mongoose = require('./db/config');
const User = require('./model/User-sample-model')(mongoose);
const faker = require('faker');
var page_content;
var comment = {title: faker.lorem.sentence(), body:faker.lorem.words(), date: Date.now()};
// var user = new User();

// user.age = 32;
// user.name.first = faker.name.firstName("male");
// user.name.last = faker.name.lastName("female");
// user.email = faker.internet.email();
// user.requisitos = "node";
// user.posts.push(comment);
//
// setTimeout(function () {
//   var validate = user.validateSync();
//   if(validate !== undefined){
//       console.log(validate.toString());
//   }else{
//     user.save((err,data)=>{
//       if(!err){
//         console.log("ook");
//       }
//
//       User.find({}).exec().then((user)=>{
//         user.forEach((iterator_user_item)=>{
//             console.log(iterator_user_item.name.initials);
//         });
//       }).catch((err)=>{console.log(err)});
//
//     });
//   }
// }, 500);


http.createServer((req,res)=>{
  var request_url = url.parse(req.url,true);
  var request_path = request_url.pathname;

  console.log("url requisitada: ",request_path);

  if(request_path == '/'){
    User.find({}).exec().then((database_users)=>{
      var html = require('./views/index_handle')(database_users);
      display_content(html, res);
    });
  }

  if(request_path == '/register'){
      var user = new User();
      user.name.first = request_url.query.name_first;
      user.name.last  = request_url.query.name_last;
      user.email      = request_url.query.email;

      if(user.validateSync() === undefined){
        user.save((err,data)=>{
          if(!err){
            return display_content("Salvo com sucesso <a href='/'>voltar</a>",res);
          }
          return display_content("Erro ao salvar <a href='/'>voltar</a>",res);
        });

      }else{
        //nao esta apresentando um comportamento como esperado
         display_content(user.validateSync().toString() + " <a href='/'>voltar</a>", res);
      }
  }
}).listen(3000);

function display_content(_content, res){
  res.writeHead((_content===undefined?'404':'200'), {'Content-type':'text/html'});
  res.end(_content);
}
