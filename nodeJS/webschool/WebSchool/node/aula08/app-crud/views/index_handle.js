'use strict';
module.exports = (user_list)=>{

  var html = '<!DOCTYPE html> <html> <head> <meta charset="utf-8"> <title>Teste</title> </head>';
  html += '<body> <h1> Olá! vamos cadastrar um usuario?</h1> <form action="/register" method="GET"> <span> Primeiro Nome: <input type="text" name="name_first" /> </span> <span> Sobrenome: <input type="text" name="name_last" /> </span> <span> Email: <input type="text" name="email" /> </span> <input type="submit" value="Salvar" /> </form>';
  html += "<ul>";
  user_list.forEach((iterator_user_item)=>{
    html += "<li>"+ iterator_user_item.name.initials+": " +iterator_user_item.email;
  });
  html += "</ul></body> </html> ";
  return html;
}
