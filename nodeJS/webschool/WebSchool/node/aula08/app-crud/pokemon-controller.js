'use strict';

const _schema = require('./schema/pokemons-schema');
const Pokemon = require('./model/pokemons')(_schema,'Pokemon');

var query = {name: /pikachu/i};

const CRUD = {
  create: function(data){
    console.log(data);
    new Pokemon(data).save((err,data)=>{
      if(err) return console.log(err);
      return console.log("inseriu ",data);
    });
  },
  retrieve: function(query){
    Pokemon.find(query,(err,data)=>{
      if(err) return console.log(err);
      return console.log("Buscou ",data);
    });
  },
  update: function(query, mod){

    Pokemon.update(query, mod,(err,data)=>{
      if(err) return console.log(err);
      return console.log("alterou ",data);
    });
  },
  delete: function(query){
    Pokemon.remove(query, (err, data)=>{
      if(err) return console.log(err);
      return console.log("Deletado ",data);
    });
  }
};

module.exports = CRUD;
