
const user_schema = require('../schema/user-schema');
module.exports = (mongoose)=>{
  var model_user =  mongoose.model('user',user_schema);

  model_user.schema.path('requisitos').validate((value)=>{
    return /js|html|css|angular|node|mongodb/i.test(value);
  },'Requisito {VALUE} inválido');

  return model_user;
}
