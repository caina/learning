var angular = angular.module("caina",[]);
angular.filter("isAdult",function(){
  return function(age){
    if(isNaN(age)) return "Digite um número";
    if(age >= 18) return "Você é maior de idade!";
    else return "Ainda um bebê";
  }
});
angular.filter("whatTimeIsIt",function(){
  return function(time){
    if(time){
      var my_time = time.split(":");
      if(!isNaN(my_time[0])){
        if(my_time[0]>=0 && my_time[0]<=12) return "Bom Dia";
        if(my_time[0]>12 && my_time[0]<=18) return "Bom Tarde";
        if(my_time[0]>18 && my_time[0]<=23) return "Bom Noite";
      }
    }
  }
});
