/*
    esse exercicio e o livro parecem estar desatualizados quanto ao jade
    
    Eu tive este erro: 
    
   8|       document.getElementById('chat').innerHTML =
   9|         '<p><b>' + data.title + '</b>: ' + data.contents + '</p>';
 > 10|     });
   11|     var submitChat = function(form) {
   12|       socket.emit('chat', {text: form.chat.value});
   13|       return false;

Unexpected token ;

    Para corrigir eu adicionei um '.' no inicio do script, isso indica ao jade que vou colocar script proprio
    
*/

const express = require('express');
const http = require('http');
var app = express();

var server = http.createServer(app);
var io = require('socket.io').listen(server);
var catchPhrases = ["Why i outghta...","Nyuk Nyuk Nyuk","Poifect!","Spread Out!","Say a few syllables","Soitenly"];

app.set('view engine','jade');
app.set('view options',{layout:true});
app.set('views',__dirname+'/views');

app.get("/stooges/chat",function(req,res,next){
   res.render('chat'); 
});
io.sockets.on('connection',function(socket){
    var sendChat = function(title, text){
        socket.emit('chat',{
            title:title,
            contents:text
        });
    };
    
    setInterval(function(){
        var randomIndex = Math.floor(Math.random()*catchPhrases.length);
        sendChat('Stooge',catchPhrases[randomIndex]);
    },5000);
    
    sendChat("Welcome to Stooge Chat","The Stooge are on the line");
    socket.on('chat',function(data){
        sendChat('You',data.text); 
    });
    
});
        
app.get('/?',function(req,res){
    res.render('index');
});
server.listen(8080);
console.log("e lá vamos nós");