# Estudos em geral de java

## Java In28minutes

documentation: https://github.com/in28minutes/SpringIn28Minutes

Jpa, Java9, Spring-boot-2,

Website to initialize spring apps
http://start.pring.io

## basic project uses:
- Web: the springboot
- DevTools: Hotreload
- Actuator: Error Log
- Config Client: ??
- Feign: to make calls between projects
- ribbon: service discovery
- Zuul: Gateway to intercept request between requests
- Zipkin: centralized Tracing errors
- sleuth: gives track id to requests spring-cloud-starter-sleuth

http://localhost:8765/currency-exchange-service/currency-exchange/from/USD/to/INR
http://localhost:8100/currency-exchange-feign/from/USD/to/INR/quantity/50000

## RabbitMQ
installing at: https://www.rabbitmq.com/install-homebrew.html

To run RabbitMQ: /usr/local/sbin/rabbitmq-server

##Zipkin
https://zipkin.io/pages/quickstart
RABBIT_URI=amqp://localhost java -jar zipkin.jar
