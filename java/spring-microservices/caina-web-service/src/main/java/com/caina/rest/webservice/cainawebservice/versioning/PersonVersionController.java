package com.caina.rest.webservice.cainawebservice.versioning;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersionController {

    @GetMapping(value= "/person/param", params="version=1")
    public PersonV1 personV1() {
        return new PersonV1("Douglas Caina");
    }

    @GetMapping(value="/person/param", params = "version=2")
    public PersonV2 personV2() {
        return new PersonV2(new Name("Douglas", "Caina"));
    }

    @GetMapping(value= "/person/header", headers="X-API-VERSION=1")
    public PersonV1 personV1Header() {
        return new PersonV1("Douglas Caina");
    }

    @GetMapping(value="/person/header", headers="X-API-VERSION=2")
    public PersonV2 personV2Header() {
        return new PersonV2(new Name("Douglas", "Caina"));
    }

    @GetMapping(value= "/person/produces", produces="application/vnd.company.app-v1+json")
    public PersonV1 personV1Produces() {
        return new PersonV1("Douglas Caina");
    }

    @GetMapping(value="/person/produces", produces="application/vnd.company.app-v2+json")
    public PersonV2 personV2Produces() {
        return new PersonV2(new Name("Douglas", "Caina"));
    }
}
