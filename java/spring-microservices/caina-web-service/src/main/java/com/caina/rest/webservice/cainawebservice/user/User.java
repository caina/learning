package com.caina.rest.webservice.cainawebservice.user;

import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "User Data")
@Entity
public class User {

    @Id
    @GeneratedValue
    private Integer id;

    @Size(min=2)
    private String name;

    @Past(message = "Data de nascimento não pode estar no passado")
    private Date birthDate;

    @OneToMany(mappedBy="user")
    private List<Post> posts;

}
