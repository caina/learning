package com.caina.rest.webservice.cainawebservice.user;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDaoService {

    private static List<User> users = new ArrayList<>();

    private static int usersCount = 3;

    static {
        users.add(new User(1, "Adam", new Date(), new ArrayList<>()));
        users.add(new User(2, "Dammit", new Date(), new ArrayList<>()));
        users.add(new User(3, "Leetch", new Date(), new ArrayList<>()));
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) {
        if (user.getId() == null) {
            user.setId(++usersCount);
        }
        users.add(user);
        return user;
    }

    public User findOne(int id) {
        return users.stream().filter(u -> u.getId() == id).findFirst().orElse(null);
    }

    public User deleteById(int userId) {

        User deleteUser = findOne(userId);

        if (deleteUser != null) {
            users = users.stream()
                    .filter(user -> !user.getId().equals(deleteUser.getId()))
                    .collect(Collectors.toList());
        }

        return deleteUser;
    }
}
