package com.caina.rest.webservice.cainawebservice.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonFilter("someBeanFilter")
public class SomeBean {


    private String field1;
    private String field2;
    private String field3;


}
