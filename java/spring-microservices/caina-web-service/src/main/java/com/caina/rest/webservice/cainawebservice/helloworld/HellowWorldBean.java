package com.caina.rest.webservice.cainawebservice.helloworld;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HellowWorldBean {

    private String message;

    public HellowWorldBean(String message) {
        setMessage(message);
    }

    @Override
    public String toString() {
        return String.format("HelloWorld Bean [message=%]", getMessage());
    }
}
