package com.caina.rest.webservice.cainawebservice.versioning;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Name {
    private String firstName;
    private String lastName;
}
