# Flutter


## Mixins
Mixins are the way to extends classes in dart, following by the syntax:
`class Person with ... {}`


## Ways to user provider
Passing the context and not attaching the content to the component
```
ChangeNotifierProvider(
    builder: (ctx)=> MyProvider
)
```


Simplified version attaching the content to the provider. Better for lists
```
ChangeNotifierProvider.value(
    value: MyProvider
)
```

## Routing:

```
MaterialApp(
      routes: {
        "/": (ctx) => TabsScreen(favoriteMeals: _favoriteMeals),
        MealsListingScreen.routeName: (ctx) =>
            MealsListingScreen(_availableMeals),
        MealsDetailsScreen.routeName: (ctx) => MealsDetailsScreen(
            toggleFavorite: _toggleFavorite, isMealFavorite: _isMealFavorite),
        FilterScreen.routeName: (ctx) =>
            FilterScreen(saveFilters: _setFilters, savedFilters: _filters),
      },
      onGenerateRoute: (settings){
        print(settings.arguments);

        if(settings.name == '/meal-detail') {
          // return the page
        }else if(settings.name == '/something-else'){
          // return MaterialPageRoute(builder: (ctx) => CategoriesScreen());
        }

        return MaterialPageRoute(builder: (ctx) => CategoriesScreen());
      },
      //  404 page
      onUnknownRoute: (settings) {
        return MaterialPageRoute(builder: (ctx) => MealsCategoriesScreen());
      },
);
```

## Providers
You can use 2 ways to read the product provider
or:
 ````
final product = Provider.of<Product>(context);
return  Image.network(
              product.imageUrl,
              fit: BoxFit.cover,
            )
```
or: 
 ```
 return Consumer<Product>(
    builder: (ctx, product, child) =>Image.network(
            product.imageUrl,
            fit: BoxFit.cover,
            label: child,
        )
    child: NEVER_CHANGE()
 )
```

using more than one provider: 

```
MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Products()),
        ChangeNotifierProvider.value(value: Cart()),
      ], 
      child: ...
      )
```

You can as well pass a provider for just a tinny part of your component by wrapping it with Consumer:
```

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);

    return ClipRRect(
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            color: Theme.of(context).accentColor,
            icon: Icon(Icons.shopping_cart),
            onPressed: () {},
          ),
          // here we are listening for events on favorite click
          leading: Consumer<Product>(
            builder: (ctx, product, child) => IconButton(
              color: Theme.of(context).accentColor,
              icon: Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
              onPressed: product.toggleFavorite,
            ),
          ),
        ),
      ),
    )}
}
```

## Keys
`key: ValueKey(id),`