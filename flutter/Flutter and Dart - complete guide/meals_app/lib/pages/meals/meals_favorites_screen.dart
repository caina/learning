import 'package:flutter/material.dart';

import '../meal.dart';
import 'components/meal_item.dart';
import 'meals_details_screen.dart';

class MealsFavoritesScreen extends StatelessWidget {
  final List<Meal> favoriteMeals;

  MealsFavoritesScreen({this.favoriteMeals});

  void _selectMeal(BuildContext context, String mealId) {
    Navigator.of(context)
        .pushNamed(MealsDetailsScreen.routeName, arguments: mealId)
  }

  @override
  Widget build(BuildContext context) {
    if (favoriteMeals.isEmpty) {
      return Center(
        child: Text("You have no favorites yet - Start adding some!"),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
            id: favoriteMeals[index].id,
            title: favoriteMeals[index].title,
            affordability: favoriteMeals[index].affordability,
            complexity: favoriteMeals[index].complexity,
            duration: favoriteMeals[index].duration,
            imageUrl: favoriteMeals[index].imageUrl,
            selectMeal: _selectMeal,
          );
        },
        itemCount: favoriteMeals.length,
      );
    }
  }
}
