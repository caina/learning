import 'package:flutter/material.dart';

import '../meal.dart';
import 'components/meal_item.dart';
import 'meals_details_screen.dart';

class MealsListingScreen extends StatefulWidget {
  static const routeName = "/cantegory-meals";

  final List<Meal> availableMeals;

  MealsListingScreen(this.availableMeals);

  @override
  _MealsListingScreenState createState() => _MealsListingScreenState();
}

class _MealsListingScreenState extends State<MealsListingScreen> {
  String categoryTitle;
  List<Meal> displayedMeals;
  bool _loadedInitState = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitState) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];

      final categoryId = routeArgs['id'];

      displayedMeals = widget.availableMeals
          .where((meal) => meal.categories.contains(categoryId))
          .toList();
      _loadedInitState = true;
    }

    super.didChangeDependencies();
  }

  void _selectMeal(BuildContext context, String mealId) {
    Navigator.of(context)
        .pushNamed(MealsDetailsScreen.routeName, arguments: mealId)
        .then((result) {
      if (result != null) {
        print(mealId);
        // _removeMeal function
        setState(() {
          displayedMeals.removeWhere((meal) => meal.id == mealId);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(categoryTitle)),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(
              id: displayedMeals[index].id,
              title: displayedMeals[index].title,
              affordability: displayedMeals[index].affordability,
              complexity: displayedMeals[index].complexity,
              duration: displayedMeals[index].duration,
              imageUrl: displayedMeals[index].imageUrl,
              selectMeal: _selectMeal);
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
