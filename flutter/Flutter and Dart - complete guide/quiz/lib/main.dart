import 'package:flutter/material.dart';
import 'package:quiz/answer.dart';
import 'package:quiz/question.dart';
import 'package:quiz/quiz.dart';
import 'package:quiz/result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _score = 0;

  void _answerQuestion(int score) {
    setState(() {
      _questionIndex = _questionIndex + 1;
      this._score += score;
      print(_questionIndex);
    });
  }

  void restart() {
    setState(() {
      this._score = 0;
      this._questionIndex = 0;
    });
  }

  final _questions = const [
    {
      'questionText': "Qual a sua cor favorita?",
      'answers': [
        {'text': "branco", 'score': 10},
        {'text': "preto", 'score': 80},
        {'text': "Rosa", 'score': 100},
      ]
    },
    {
      'questionText': "Qual seu animal favorito?",
      'answers': [
        {'text': "cachorro", 'score': 10},
        {'text': "gato", 'score': 80},
      ]
    },
    {
      'questionText': "Quem contruiu berlim?",
      'answers': [
        {'text': "Turcos", 'score': 80},
        {'text': "Turquetinhos", 'score': 180}
      ],
    },
    {
      'questionText': "Quantos anos tem dois irmãos?",
      'answers': [
        {'text': "34", 'score': 34},
        {'text': "56", 'score': 56},
        {'text': "67", 'score': 67},
      ]
    }
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Quiz app',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
            appBar: AppBar(
                title: Text("Quiz app - Score: " + this._score.toString())),
            body: _questionIndex < _questions.length
                ? Quiz(
                    question: _questions[_questionIndex],
                    answerQuestion: _answerQuestion)
                : Result(score: _score, restart: restart,)));
  }
}
