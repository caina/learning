import 'package:flutter/material.dart';
import 'package:quiz/question.dart';

import 'answer.dart';

class Quiz extends StatelessWidget {
  final Map<String, Object> question;
  final Function answerQuestion;

  Quiz({@required this.question, @required this.answerQuestion});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(questionText: question['questionText']),
        ...(question['answers'] as List<Map<String, Object>>)
            .map((answer) => new Answer(
                text: answer['text'],
                score: answer['score'],
                callback: answerQuestion))
            .toList()
      ],
    );
  }
}
