import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final score;
  final Function restart;

  Result({this.score, this.restart});

  String get resultPhrase {
    return "Your score is: " + score.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text("Restart"),
            textColor: Colors.blue,
            onPressed: restart,
          )
        ],
      ),
    );
  }
}
