
job: &nome-job
  - stage: test
  - script:
    - do something

copy-job: *nome-job
(vai printar aqui o que tem no job anterior)

caso queria apenas extender de outro job, só fazer assim:

copy-job: 
  <<: *nome-job
  - script:
    - other thing
