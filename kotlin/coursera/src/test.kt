fun fibonacci(): Sequence<Int> = sequence {
    yield(0)
    yield(1)

    var x = 1
    var y = 0
    while(true) {
        val newV = x+y

        y = x
        x = newV

        yield(newV)
    }
}

fun main(args: Array<String>) {
//    fibonacci().take(4).toList().toString() eq
//            "[0, 1, 1, 2]"
//
//    fibonacci().take(10).toList().toString() eq
//            "[0, 1, 1, 2, 3, 5, 8, 13, 21, 34]"

    foo(X, Y);
}

infix fun <T> T.eq(other: T) {
    if (this == other) println("OK")
    else println("Error: $this != $other")
}

interface X {
    var first: Int
    var second: Int
    var third: Int
}

interface Y {
    fun start()
    fun finish()
}

interface Z {
    fun init()
}

fun foo(x: X, y: Y?, z: Z) {
    x.apply {
        first = 1
        second = 2
        third = 3
    }
    y?.run {
        this.start()
        this.finish()
    }
    val result = with(z) {
        init()
        this
    }
}