package board

import board.Direction.*

fun createSquareBoard(width: Int): SquareBoard = SquareBoardImp(width)
fun <T> createGameBoard(width: Int): GameBoard<T> = GameBoardImp(width)

open class SquareBoardImp(final override val width: Int) : SquareBoard {

    private val cells: List<Cell> = sequence {
        var i = 0
        repeat(width) {
            var j = 0
            i++
            repeat(width) {
                j++
                yield(Cell(i, j))
            }
        }
    }.toList()

    private fun List<Cell>.find(i: Int, j: Int) = this.find { it.i == i && it.j == j }
    
    override fun getAllCells(): Collection<Cell> {
        return cells
    }

    override fun getCell(i: Int, j: Int): Cell = cells.find(i, j)!!

    override fun getCellOrNull(i: Int, j: Int): Cell? = cells.find(i, j)

    override fun getColumn(iRange: IntProgression, j: Int): List<Cell> {
        return sequence {
            for (i in iRange) {
                getCellOrNull(i, j)?.let { yield(it) }
            }
        }.toList()
    }

    override fun getRow(i: Int, jRange: IntProgression): List<Cell> {
        return sequence {
            for (j in jRange) {
                getCellOrNull(i, j)?.let { yield(it) }
            }
        }.toList()
    }

    override fun Cell.getNeighbour(direction: Direction): Cell? {
        val (i, j) = when (direction) {
            UP -> i - 1 to j
            DOWN -> i + 1 to j
            RIGHT -> i to j + 1
            LEFT -> i to j - 1
        }
        return cells.find(i, j)
    }
}

class GameBoardImp<T>(size: Int) : SquareBoardImp(size), GameBoard<T> {

    private var values: Set<Pair<Cell, T?>> = this.getAllCells().map { it to null }.toSet()

    override fun get(cell: Cell): T? = values.find { it.first == cell }!!.second

    override fun set(cell: Cell, value: T?) {
        values = values.map {
            val updatedValue = if (it.first == cell) value else it.second

            it.first to updatedValue
        }.toSet()
    }

    override fun filter(predicate: (T?) -> Boolean): Collection<Cell> {
        return values.filter { predicate(it.second) }.map { it.first }
    }

    override fun find(predicate: (T?) -> Boolean): Cell? {
        return values.find { predicate(it.second) }?.first
    }

    override fun any(predicate: (T?) -> Boolean): Boolean {
        return values.any { predicate(it.second) }
    }

    override fun all(predicate: (T?) -> Boolean): Boolean {
        return values.all { predicate(it.second) }
    }
}