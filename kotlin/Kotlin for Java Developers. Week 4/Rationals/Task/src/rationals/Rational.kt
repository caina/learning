package rationals

import java.math.BigInteger.ONE
import java.math.BigInteger.ZERO
import java.math.BigInteger

class Rational(val numerator: BigInteger, val denominator: BigInteger) {

    companion object {
        private fun minCommonDivisor(denominatorA: BigInteger, denominatorB: BigInteger): BigInteger {
            val maxCommonDivisor = denominatorA.gcd(denominatorB)
            return denominatorA * (denominatorB / maxCommonDivisor)
        }

        fun toSameBase(first: Rational, second: Rational): Pair<Rational, Rational> {
            val mmc = minCommonDivisor(first.denominator, second.denominator)

            val firstEquivalentRational = Rational(first.toEquivalentNumerator(mmc), mmc)
            val secondEquivalentRational = Rational(second.toEquivalentNumerator(mmc), mmc)

            return firstEquivalentRational to secondEquivalentRational
        }
    }

    fun toEquivalentNumerator(mmc: BigInteger) = this.numerator * (mmc / this.denominator)

    /*
    The denominator must be always positive in the normalized form.
    If the negative rational is normalized, then only the numerator can be negative,
    e.g. the normalized form of 1/-2 should be -1/2.wq
    */
    fun normalize(): Rational {
        if (numerator == ZERO) return this

        val maxDivider = numerator.gcd(denominator)
        val normalized = Rational(
                numerator = numerator / maxDivider,
                denominator = (denominator / maxDivider).abs()
        )

        return when {
            denominator < ZERO -> -normalized
            else -> normalized
        }
    }

    override fun toString(): String {
        return when (denominator) {
            ONE -> numerator.toString()
            else -> "$numerator/$denominator"
        }
    }

    override fun equals(other: Any?) = when (other) {
        is Rational -> this.numerator == other.numerator && this.denominator == other.denominator
        else -> super.equals(other)
    }

    override fun hashCode(): Int {
        var result = numerator.hashCode()
        result = 31 * result + denominator.hashCode()
        return result
    }

    operator fun compareTo(rational: Rational): Int {
        val (sameBaseThis, sameBaseTarget) = toSameBase(this, rational)

        return when {
            sameBaseThis.numerator > sameBaseTarget.numerator -> 1
            sameBaseThis.numerator < sameBaseTarget.numerator -> -1
            else -> 0
        }
    }

    operator fun unaryMinus(): Rational {
        return Rational(-numerator, denominator)
    }

    operator fun rangeTo(rational: Rational): Pair<Rational, Rational> {
        return Pair(this, rational)
    }

    operator fun plus(target: Rational): Rational {
        val (sameBaseThis, sameBaseTarget) = toSameBase(this, target)
        return Rational(
                numerator = sameBaseThis.numerator + sameBaseTarget.numerator,
                denominator = sameBaseThis.denominator
        ).normalize()
    }

    operator fun minus(target: Rational): Rational {
        val (sameBaseThis, sameBaseTarget) = toSameBase(this, target)
        return Rational(
                numerator = sameBaseThis.numerator - sameBaseTarget.numerator,
                denominator = sameBaseThis.denominator
        ).normalize()
    }

    operator fun times(target: Rational) = Rational(
            numerator = this.numerator * target.numerator,
            denominator = this.denominator * target.denominator
    ).normalize()

    operator fun div(target: Rational) = Rational(
            numerator = this.numerator * target.denominator,
            denominator = this.denominator * target.numerator).normalize()
}

operator fun Pair<Rational, Rational>.contains(rational: Rational): Boolean {
    return rational >= this.first && rational < this.second
}

fun String.toRational(): Rational {
    return if (this.contains("/")) {
        val (num, den) = this.split("/")
        Rational(num.toBigInteger(), den.toBigInteger()).normalize()
    } else {
        Rational(this.toBigInteger(), ONE)
    }
}

infix fun Number.divBy(first: Number) = Rational(this.toString().toBigInteger(), first.toString().toBigInteger()).normalize()

fun main() {
    val half = 1 divBy 2
    val third = 1 divBy 3

    val sum: Rational = half + third
    println(5 divBy 6 == sum)

    val difference: Rational = half - third
    println(1 divBy 6 == difference)

    val product: Rational = half * third
    println(1 divBy 6 == product)

    val quotient: Rational = half / third
    println(3 divBy 2 == quotient)

    val negation: Rational = -half
    println(-1 divBy 2 == negation)

    println((2 divBy 1).toString() == "2")
    println((-2 divBy 4).toString() == "-1/2")
    println("117/1098".toRational().toString() == "13/122")

    val twoThirds = 2 divBy 3
    println(half < twoThirds)

    println(half in third..twoThirds)

    println(2000000000L divBy 4000000000L == 1 divBy 2)

    println("912016490186296920119201192141970416029".toBigInteger() divBy
            "1824032980372593840238402384283940832058".toBigInteger() == 1 divBy 2)
}





