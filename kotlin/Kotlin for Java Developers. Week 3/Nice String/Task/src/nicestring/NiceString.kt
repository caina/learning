package nicestring

// condition 1
// It doesn't contain substrings bu, ba or be;

// condition 2
// It contains at least three vowels (vowels are a, e, i, o and u);

// condition 3
//It contains a double letter (at least two similar letters following one another), like b in "abba".
val VOWELS = listOf('a', 'e', 'i', 'o', 'u')
val forbiddenWords = listOf("ba", "be", "bu")

fun String.isNice(): Boolean {
    var total = 0

    val hasNotForbiddenWords = { input: String -> if (forbiddenWords.none{ input.contains(it)}) 1 else 0 }
    val hasMoreThenThreeVOWELS = { input: String -> if (input.count(VOWELS::contains) >= 3) 1 else 0 }
    val hasDoubleLetters = { input: String -> if (input.zipWithNext().count { it.first == it.second } > 0) 1 else 0 }

    total += hasNotForbiddenWords(this)
    total += hasMoreThenThreeVOWELS(this)
    total += hasDoubleLetters(this)

    return total > 1
}


