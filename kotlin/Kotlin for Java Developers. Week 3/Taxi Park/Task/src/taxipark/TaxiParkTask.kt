package taxipark

import java.util.*
import kotlin.math.ceil
import kotlin.math.roundToInt

/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> = allDrivers.minus(trips.map { it.driver }.toSet())

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> {
    val byPassenger = trips.map { it.passengers }

    return allPassengers.filter {
        byPassenger.count { trip -> trip.contains(it) } >= minTrips
    }.toSet()
}

/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> = trips
        .filter { it.driver == driver }
        .flatMap { it.passengers }
        .groupBy { it }
        .filter { it.value.size > 1 }
        .flatMap { it.value }
        .distinct()
        .toSet()

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
private fun Passenger.hadMoreTripsWithDiscount(trips: List<Trip>): Boolean {
    val totalRides = trips.filter { it.passengers.contains(this) }.map { it.discount }
    val ridesWithoutDiscount = totalRides.filter(Objects::isNull).size
    val ridesWithDiscount = totalRides.filter(Objects::nonNull).size

    return ridesWithDiscount > ridesWithoutDiscount
}

fun TaxiPark.findSmartPassengers(): Set<Passenger> = allPassengers.filter { it.hadMoreTripsWithDiscount(trips) }.toSet()

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    if (this.trips.isEmpty()) {
        return null
    }

    val maxDuration: Int = trips.map { it.duration }.max() ?: 0
    val rangesByTrips = HashMap<Int, IntRange>()
    for (i in 0..maxDuration step 10) {
        val range = IntRange(i, i + 9)
        val trips = this.trips.filter { it.duration in range }.count()
        rangesByTrips[trips] = range
    }

    return rangesByTrips[rangesByTrips.toSortedMap().lastKey()]
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    if(this.trips.isEmpty()) {
        return false
    }
    val incomeByDriver = trips
            .groupBy { it.driver }
            .map { it.value.map { that -> that.cost }.sum()}
            .sortedByDescending { it }

    val topTwentyDrivers = (allDrivers.size * 0.2).roundToInt()
    var income = 0.0
    for (i in 0 until topTwentyDrivers) {
        income += incomeByDriver[i]
    }

    val total = trips.map { it.cost }.sum()
    val percentage = income * 100 / total
    return percentage >= 80
}
