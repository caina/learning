package mastermind

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret: String, guess: String): Evaluation {
    val totalSecret: MutableList<Pair<Int, Char>> = secret.subtract(guess)
    val totalGuess: MutableList<Pair<Int, Char>> = guess.subtract(secret)

    return Evaluation(
            rightPosition = calculateRightGuess(secret, totalSecret),
            wrongPosition = calculateWrongPositions(totalSecret, totalGuess)
    )
}

private fun calculateRightGuess(secret: String, totalSecret: MutableList<Pair<Int, Char>>) = secret.length - totalSecret.size
private fun calculateWrongPositions(totalSecret: MutableList<Pair<Int, Char>>, totalGuess: MutableList<Pair<Int, Char>>): Int {
    var wrong = 0
    totalSecret.forEach {
        val found = totalGuess.findChar(it.second)
        if (found != null) {
            wrong++
            totalGuess.remove(found)
        }
    }
    return wrong
}

private fun String.asIndexMap() = this.toCharArray().mapIndexed { index, c -> index to c }.toMutableList()
private fun String.subtract(from: String) = this.asIndexMap().minus(from.asIndexMap()).toMutableList()
private fun MutableList<Pair<Int, Char>>.findChar(needle: Char) = this.find { it.second == needle }