import {
  Component,
  OnInit,
  AfterContentInit,
  // ContentChild,
  ContentChildren,
  QueryList,
  OnDestroy
} from '@angular/core';
import { TabComponent } from "app/tab/tab.component";
import { Tab } from "../tab/tab.interface";


@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterContentInit, OnDestroy {

  // @ContentChild(TabComponent) tab: TabComponent;
  private tabClickSubscriptions: any[] = [];
  private tabClickSubscription: any;
  @ContentChildren(TabComponent) public tabs: QueryList<TabComponent>;

  constructor() { }

  ngAfterContentInit(): void {
    this.tabs.forEach(tab => {
      let subscription = tab.onclick.subscribe(() => {
        console.log(`tab: ${tab.title} content click`);
      })
      this.tabClickSubscriptions.push(subscription);
    });
    this.selectTab(this.tabs.first);

    // if (this.tab) {
    //   console.log(this.tab);
    //   this.addTab(this.tab);
    //   this.tabClickSubscription = this.tab.onclick.subscribe(() => console.log("clicoooou"));
    // }
  }

  ngOnDestroy(): void {
    // this.tabClickSubscription.unsubscribe();
    if (this.tabClickSubscriptions) {
      this.tabClickSubscriptions.forEach(item => item.unsubscribe());
    }
  }

  ngOnInit() {
  }

  addTab(tab: Tab) {
    // if (this.tabs.length === 0) {
    //   tab.isActive = true;
    // }
    // this.tabs.push(tab);
  }

  selectTab(tab: Tab) {
    // for (let tab of this.tabs) {
    //   tab.isActive = false;
    // }
    // tab.isActive = true;

    this.tabs.forEach(tab => tab.isActive = false);
    tab.isActive = true;
  }


}
