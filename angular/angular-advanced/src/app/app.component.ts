import { Component, ViewChild, AfterContentInit, ViewChildren, QueryList, AfterViewInit, ChangeDetectorRef, ElementRef, Renderer2 } from '@angular/core';
import { SimpleAlertViewComponent } from 'app/simple-alert-view/simple-alert-view.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentInit, AfterViewInit {

  public isAddTimerVisible: boolean = false;
  // public isEndTimerAlertVisible:boolean = false;
  public time: number = 0;
  public timers: Array<number> = [];

  // @ViewChild(SimpleAlertViewComponent) alert: SimpleAlertViewComponent;
  @ViewChildren(SimpleAlertViewComponent) alerts: QueryList<SimpleAlertViewComponent>;
  @ViewChild("timeInput") timeInput: ElementRef;

  constructor(private cdRef: ChangeDetectorRef, private renderer:Renderer2) {
    this.timers = [3, 20, 185];
  }

  ngAfterViewInit(): void {

    // AOT
    this.renderer.setAttribute(this.timeInput.nativeElement, "placeholder", "Enter Seconds");
    this.renderer.addClass(this.timeInput.nativeElement,"time-in");
    // JIT
    // this.timeInput.nativeElement.setAttribute("placeholder", "Enter Seconds");
    // this.timeInput.nativeElement.classList.add("time-in");

    this.alerts.forEach(item => {
      if (!item.title) {
        item.title = "Hi!";
        item.message = "Hhellooowwww";
      }
    });

    this.cdRef.detectChanges();
  }

  ngAfterContentInit(): void {
    // this.alert.title="Hi!";
    // this.alert.message = "Hello World";
    // this.alert.show();
  }

  public showAddTimer() {
    this.isAddTimerVisible = true;
    setTimeout(() => {
      // this.timeInput.nativeElement.focus();
      // AOT
      this.renderer.selectRootElement(this.timeInput.nativeElement).focus();
    });
  }

  public hideAddTimer() {
    this.isAddTimerVisible = false;
  }

  public showEndTimerAlert() {
    // this.isEndTimerAlertVisible = true;
   this.alerts.first.show();
  }
  public hideEndTimerAlert() {
    // this.isEndTimerAlertVisible = false;
  }

  public submitAddTimer() {
    this.timers.push(this.time);
    this.hideAddTimer();
  }

}
