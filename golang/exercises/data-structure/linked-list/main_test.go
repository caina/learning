package linked_list_test

import (
	. "github.com/golang-examples/data-structure/linked-list"
	"testing"
)

func TestHasHead(t *testing.T) {
	list := &List{}
	list.Append("data")

	if list.Head() == nil {
		t.Error("Head must not be nil")
	}
	if list.Head().Value != "data" {
		t.Error("Head should be data")
	}
}

func TestMustAppend(t *testing.T) {
	list := &List{}
	list.Append("first")
	list.Append("second")

	if list.Head().Next.Value != "second" {
		t.Error("second element should be `second`")
	}
}

func TestMustAppend3Itens(t *testing.T) {
	list := &List{}
	list.Append("first")
	list.Append("second")
	list.Append("third")

	if list.Head().Next.Next.Value != "third" {
		t.Error("The last element should be third")
	}
}

func TestAppendShouldAddPrev(t *testing.T) {
	list := &List{}
	list.Append("first")
	list.Append("second")

	if list.Head().Next.Prev.Value != "first" {
		t.Error("prev element should be first")
	}
}

func TestInsertWithEmptyList(t *testing.T) {
	list := &List{}
	if err := list.Insert("me", 3); err == nil {
		t.Error("Insert should give an error")
	}
}

func TestInsertAtHead(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")
	list.Append("três")
	list.Append("quatorze")

	if err := list.Insert("newOne", 0); err != nil {
		t.Error("Error should be nil")
	}

	if list.Head().Value != "newOne" {
		t.Error("Insert did not work")
	}

	if list.Head().Next.Value != "uno" {
		t.Error("The previous head is not present")
	}
}

func TestInsertSecondElement(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")
	list.Append("três")

	if err := list.Insert("correctOne", 2); err != nil {
		t.Error("Insert should not give error")
	}

	secondNode := list.Head().Next
	if secondNode.Value != "correctOne" {
		t.Error("Second node is not correct")
	}
}

func TestInsertSecondElementPrevElement(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")
	list.Append("três")

	if err := list.Insert("correctOne", 2); err != nil {
		t.Error("Insert should not give error")
	}

	secondNode := list.Head().Next.Next.Prev
	if secondNode.Value != "correctOne" {
		t.Error("Third element Prev is not correct")
	}
}

func TestShouldRemoveHead(t *testing.T) {
	list := &List{}
	list.Append("uno")

	_ = list.RemoveAt(0)
	if list.Head() != nil {
		t.Error("Header was not deleted")
	}
}

func TestShouldRemoveIndex(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")
	list.Append("tres")

	_ = list.RemoveAt(2)
	if list.Head().Next.Value != "tres" {
		t.Error("second element was not deleted")
	}

	if list.Head().Next.Prev.Value != "uno" {
		t.Error("Prev should be uno ")
	}
}

func TestIndexOfHead(t *testing.T) {
	list := &List{}
	list.Append("uno")

	if node, _ := list.IndexOf(1); node.Value != "uno" {
		t.Error("Head not reached")
	}
}

func TestIndexOfFirstElement(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")

	if node, _ := list.IndexOf(2); node.Value != "dos" {
		t.Error("first not reached")
	}
}

func TestIndexOfoutOfBounds(t *testing.T) {
	list := &List{}
	list.Append("uno")
	list.Append("dos")

	if _, err := list.IndexOf(10); err == nil {
		t.Error("should give error")
	}
}
