package linked_list

import "github.com/pkg/errors"

type List struct {
	head *Nodes
	size int
	Tail *Nodes
}

type Nodes struct {
	Value string
	Prev  *Nodes
	Next  *Nodes
}

func (l *List) Head() *Nodes {
	return l.head
}

func (l *List) Append(val string) {
	newNode := &Nodes{
		Value: val,
		Prev:  nil,
		Next:  nil,
	}

	if l.head == nil {
		l.head = newNode
	} else {
		last := l.head
		for {
			if last.Next == nil {
				break
			}
			last = last.Next
		}
		newNode.Prev = last
		last.Next = newNode
	}

	l.size++
}

func (l *List) Insert(val string, index int) error {
	newNode := &Nodes{Value: val, Prev: nil, Next: nil}

	if l.IsEmpty() {
		return errors.New("Index out of bounds")
	}

	if index == 0 {
		newNode.Next = l.head
		l.head = newNode
	} else {
		last := l.head
		for i := 1; i < index-1; i++ {
			if last.Next == nil {
				return errors.New("Index out of bounds")
			}
			last = last.Next
		}

		newNode.Next = last.Next
		last.Next.Prev = newNode
		last.Next = newNode
	}

	l.size++
	return nil
}

func (l *List) RemoveAt(index int) error {
	if l.IsEmpty() {
		return errors.New("Index out of bounds")
	}

	var next *Nodes
	if index == 0 {
		next = l.head.Next
		l.head = next
	} else {
		next = l.head
		for i := 1; i < index-1; i++ {
			if next.Next == nil {
				return errors.New("Index out of bounds")
			}
			next = next.Next
		}

		next.Next = next.Next.Next
		next.Next.Prev = next
	}

	l.size--
	return nil
}

func (l *List) IndexOf(index int) (*Nodes, error) {
	var node *Nodes = nil

	for i := 0; i < index; i++ {
		if node == nil {
			node = l.head
		} else {
			if node.Next == nil {
				return nil, errors.New("index out of bounds")
			}
			node = node.Next
		}
	}

	return node, nil
}

func (l *List) IsEmpty() bool {
	return l.size == 0
}

func (l *List) Size() int {
	return l.size
}
